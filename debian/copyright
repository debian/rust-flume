Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Flume
Upstream-Contact: https://github.com/zesterer/flume/issues
Source: https://github.com/zesterer/flume

Files: *
Copyright:
  Joshua Barretto <joshua.s.barretto@gmail.com>
License-Grant:
 Flume is licensed under either of:
  * Apache License 2.0, (<http://www.apache.org/licenses/LICENSE-2.0>)
  * MIT license (<http://opensource.org/licenses/MIT>)
License: Apache-2.0 or Expat
Reference:
 Cargo.toml
 LICENSE-APACHE
 LICENSE-MIT
 README.md

Files:
 tests/golang.rs
Copyright:
  2009  The Go Authors
License: BSD-3-clause~Google
Reference:
 <https://golang.org/LICENSE>
 FIXME

Files:
 tests/mpsc.rs
Copyright:
  2013-2014  The Rust Project Developers
License-Grant:
 Apache License, Version 2.0
 or MIT license,
 at your option
License: Apache-2.0 or Expat

Files:
 debian/*
Copyright:
  2022-2025  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This packaging is free software:
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3,
 or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

License: Apache-2.0
Reference: /usr/share/common-licenses/Apache-2.0

License: BSD-3-clause~Google
 Redistribution and use in source and binary forms,
 with or without modification,
 are permitted
 provided that the following conditions are met:
   * Redistributions of source code must retain
     the above copyright notice, this list of conditions
     and the following disclaimer.
   * Redistributions in binary form must reproduce
     the above copyright notice, this list of conditions
     and the following disclaimer
     in the documentation and/or other materials
     provided with the distribution.
   * Neither the name of Google Inc.
     nor the names of its contributors
     may be used to endorse or promote
     products derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED
 BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files
 (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3
